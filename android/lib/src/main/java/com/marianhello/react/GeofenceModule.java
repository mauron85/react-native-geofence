package com.marianhello.react;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.cowbell.cordova.geofence.GeoNotification;
import com.cowbell.cordova.geofence.Gson;
import com.cowbell.cordova.geofence.Logger;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by finch on 20.10.2016.
 */

public class GeofenceModule extends ReactContextBaseJavaModule implements LifecycleEventListener {
    public static final String TAG = "GeofencePlugin";
    private GeoNotificationManager geoNotificationManager;
    private Context context;
    protected static Boolean isInBackground = true;

    private class Action {
        public String action;
        public JSONArray args;
        public Callback callback;

        public Action(String action,JSONArray args,Callback callback) {
            this.action = action;
            this.args = args;
            this.callback = callback;
        }
    }

    //FIXME: what about many executedActions at once
    private Action executedAction;

    public GeofenceModule(ReactApplicationContext reactContext) {
        super(reactContext);
        Logger.setLogger(new Logger(TAG, context, false));
        geoNotificationManager = new GeoNotificationManager(context);
    }

    @ReactMethod
    public void addOrUpdate(ReadableMap options, Callback callback) {
        List<GeoNotification> geoNotifications = new ArrayList<GeoNotification>();
        for (int i = 0; i < args.length(); i++) {
            GeoNotification not = parseFromJSONObject(args.getJSONObject(i));
            if (not != null) {
                geoNotifications.add(not);
            }
        }
        geoNotificationManager.addGeoNotifications(geoNotifications,
                callback);

    }

    @ReactMethod
    public void remove(ReadableArray args, Callback callback) {
        List<String> ids = new ArrayList<String>();
        for (int i = 0; i < args.size(); i++) {
            ids.add(args.getString(i));
        }
        geoNotificationManager.removeGeoNotifications(ids, callback);
    }

    @ReactMethod
    public void removeAll(Callback callback) {
        geoNotificationManager.removeAllGeoNotifications(callback);
    }

    @ReactMethod
    public void getWatched(Callback callback) {
        List<GeoNotification> geoNotifications = geoNotificationManager
                .getWatched();
        callback.invoke(Gson.get().toJson(geoNotifications));
    }

    @ReactMethod
    public void initialize(Callback callback) {
        String[] permissions = {
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        };

        if (!hasPermissions(permissions)) {
//            PermissionHelper.requestPermissions(this, 0, permissions);
        } else {
            callback.invoke();
        }
    }

    @ReactMethod
    public void deviceReady(Callback callback) {
        deviceReady();
    }

    public static void onTransitionReceived(List<GeoNotification> notifications) {
        Log.d(TAG, "Transition Event Received!");
        String js = "setTimeout('geofence.onTransitionReceived("
                + Gson.get().toJson(notifications) + ")',0)";
        if (webView == null) {
            Log.d(TAG, "Webview is null");
        } else {
            webView.sendJavascript(js);
        }
    }

    private GeoNotification parseFromJSONObject(JSONObject object) {
        GeoNotification geo = null;
        geo = GeoNotification.fromJson(object.toString());
        return geo;
    }

    private void deviceReady() {
        //TODO: implement
    }

    private boolean hasPermissions(String[] permissions) {
//        for (String permission : permissions) {
//            if (!PermissionHelper.hasPermission(this, permission)) {
//                return false;
//            }
//        }

        return true;
    }

//    public void onRequestPermissionResult(int requestCode, String[] permissions,
//                                          int[] grantResults) throws JSONException {
//        PluginResult result;
//
//        if (executedAction != null) {
//            for (int r:grantResults) {
//                if (r == PackageManager.PERMISSION_DENIED) {
//                    Log.d(TAG, "Permission Denied!");
//                    result = new PluginResult(PluginResult.Status.ILLEGAL_ACCESS_EXCEPTION);
//                    executedAction.callbackContext.sendPluginResult(result);
//                    executedAction = null;
//                    return;
//                }
//            }
//            Log.d(TAG, "Permission Granted!");
//
//            //try to execute method once again
//            execute(executedAction);
//            executedAction = null;
//        }
//    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {

    }

    @Override
    public String getName() {
        return null;
    }
}
