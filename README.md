# React Native Geofence Plugin

## Fork notice
This is fork of [Cordova Geofence Plugin](https://github.com/cowbell/cordova-plugin-geofence) for React Native.
Currently work in progress and Android only.
All credits to [Cowbell Labs](https://github.com/cowbell).

Plugin to monitor circular geofences using mobile devices. The purpose is to notify user if crossing the boundary of the monitored geofence.

*Geofences persist after device reboot. You do not have to open your app first to monitor added geofences*

## Installation

From master
```
npm install https://github.com/mauron85/react-native-geofence --save
```

Latest stable version

```
npm install react-native-mauron85-geofence --save
```

## Supported Platforms

- Android
- iOS (will be supported later)

# Using the plugin

React Native initialize plugin to `geofence` object.

## Methods

All methods returning promises, but you can also use standard callback functions.

- `geofence.initialize(onSuccess, onError)`
- `geofence.addOrUpdate(geofences, onSuccess, onError)`
- `geofence.remove(geofenceId, onSuccess, onError)`
- `geofence.removeAll(onSuccess, onError)`
- `geofence.getWatched(onSuccess, onError)`

For listening of geofence transistion you can override onTransitionReceived method
- `geofence.onTransitionReceived(geofences)`

## Constants

- `TransitionType.ENTER` = 1
- `TransitionType.EXIT` = 2
- `TransitionType.BOTH` = 3

## Plugin initialization

```javascript
import geofence from 'react-native-mauron85-geofence';

// geofence is now available
geofence.initialize().then(function () {
    console.log("Successful initialization");
}, function (error) {
    console.log("Error", error);
});
```

Initialization process is responsible for requesting neccessary permissions.
If required permissions are not granted then initialization fails with error message.

## Adding new geofence to monitor

```javascript
geofence.addOrUpdate({
    id:             String, //A unique identifier of geofence
    latitude:       Number, //Geo latitude of geofence
    longitude:      Number, //Geo longitude of geofence
    radius:         Number, //Radius of geofence in meters
    transitionType: Number, //Type of transition 1 - Enter, 2 - Exit, 3 - Both
    notification: {         //Notification object
        id:             Number, //optional should be integer, id of notification
        title:          String, //Title of notification
        text:           String, //Text of notification
        smallIcon:      String, //Small icon showed in notification area, only res URI
        icon:           String, //icon showed in notification drawer
        openAppOnClick: Boolean,//is main app activity should be opened after clicking on notification
        vibration:      [Integer], //Optional vibration pattern - see description
        data:           Object  //Custom object associated with notification
    }
}).then(function () {
    console.log('Geofence successfully added');
}, function (reason) {
    console.log('Adding geofence failed', reason);
});
```
Adding more geofences at once
```javascript
geofence.addOrUpdate([geofence1, geofence2, geofence3]);
```

Geofence overrides the previously one with the same `id`.

*All geofences are stored on the device and restored to monitor after device reboot.*

Notification overrides the previously one with the same `notification.id`.

## Notification vibrations

You can set vibration pattern for the notification or disable default vibrations.

To change vibration pattern set `vibrate` property of `notification` object in geofence.

###Examples

```
//disable vibrations
notification: {
    vibrate: [0]
}
```

```
//Vibrate for 1 sec
//Wait for 0.5 sec
//Vibrate for 2 sec
notification: {
    vibrate: [1000, 500, 2000]
}
```

###Platform quirks

Fully working only on Android.

On iOS vibration pattern doesn't work. Plugin only allow to vibrate with default system pattern.

## Notification icons

To set notification icons use `icon` and `smallIcon` property in `notification` object.

As a value you can enter:
- name of native resource or your application resource e.g. `res://ic_menu_mylocation`, `res://icon`, `res://ic_menu_call`
- relative path to file in `www` directory e.g. `file://img/ionic.png`

`smallIcon` - supports only resources URI

###Examples

```
notification: {
    smallIcon: 'res://my_location_icon',
    icon: 'file://img/geofence.png'
}
```

###Platform quirks

Works only on Android platform so far.

## Removing

Removing single geofence
```javascript
geofence.remove(geofenceId)
    .then(function () {
        console.log('Geofence sucessfully removed');
    }
    , function (reason){
        console.log('Removing geofence failed', reason);
    });
```
Removing more than one geofence at once.
```javascript
geofence.remove([geofenceId1, geofenceId2, geofenceId3]);
```

## Removing all geofences

```javascript
geofence.removeAll()
    .then(function () {
        console.log('All geofences successfully removed.');
    }
    , function (reason) {
        console.log('Removing geofences failed', reason);
    });
```

## Getting watched geofences from device

```javascript
geofence.getWatched().then(function (geofencesJson) {
    var geofences = JSON.parse(geofencesJson);
});
```

## Listening for geofence transitions

```javascript
geofence.onTransitionReceived = function (geofences) {
    geofences.forEach(function (geo) {
        console.log('Geofence transition detected', geo);
    });
};
```

## Listening for geofence transitions in native code

### Android

For android plugin broadcasting intent `com.cowbell.cordova.geofence.TRANSITION`. You can implement your own `BroadcastReceiver` and start listening for this intent.

Register receiver in `AndroidManifest.xml`

```xml
<receiver android:name="YOUR_APP_PACKAGE_NAME.TransitionReceiver">
    <intent-filter>
        <action android:name="com.cowbell.cordova.geofence.TRANSITION" />
    </intent-filter>
</receiver>
```

Example `TransitionReceiver.java` code

```java
......
import com.cowbell.cordova.geofence.Gson;
import com.cowbell.cordova.geofence.GeoNotification;

public class TransitionReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String error = intent.getStringExtra("error");

        if (error != null) {
            //handle error
            Log.println(Log.ERROR, "YourAppTAG", error);
        } else {
            String geofencesJson = intent.getStringExtra("transitionData");
            GeoNotification[] geoNotifications = Gson.get().fromJson(geofencesJson, GeoNotification[].class);
            //handle geoNotifications objects
        }
    }
}
```

## When the app is opened via Notification click

Android, iOS only

```javascript
geofence.onNotificationClicked = function (notificationData) {
    console.log('App opened from Geo Notification!', notificationData);
};
```

#Example usage

Adding geofence to monitor entering Gliwice city center area of radius 3km

```javascript
geofence.addOrUpdate({
    id:             "69ca1b88-6fbe-4e80-a4d4-ff4d3748acdb",
    latitude:       50.2980049,
    longitude:      18.6593152,
    radius:         3000,
    transitionType: TransitionType.ENTER,
    notification: {
        id:             1,
        title:          "Welcome in Gliwice",
        text:           "You just arrived to Gliwice city center.",
        openAppOnClick: true
    }
}).then(function () {
    console.log('Geofence successfully added');
}, function (reason) {
    console.log('Adding geofence failed', reason);
})
```

# Platform specifics

##Android

This plugin uses Google Play Services so you need to have it installed on your device.

##iOS

Plugin is written in Swift. All xcode project options to enable swift support are set up automatically after plugin is installed.

# Development

##Installation

- git clone https://github.com/mauron85/react-native-geofence
- change into the new directory
- `npm install`

##License

This software is released under the [Apache 2.0 License](http://opensource.org/licenses/Apache-2.0).

© 2014-2016 Cowbell-labs. All rights reserved
